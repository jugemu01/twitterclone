Rails.application.routes.draw do
  get 'relationships/create'
  get 'relationships/destroy'
  get 'comments/create'
  get 'comments/destroy'
  get 'favorites/create'
  get 'favorites/destroy'
  devise_for :users
  root 'tweets#index'
  resources :users do
    resource :relationships, only: [:create, :destroy]
    member do
      get 'follows'
      get 'followers'
    end
  end
  resources :tweets do
    resource :favorites, only: [:create, :destroy]
    resource :comments, only: [:create, :destroy]
  end
end
