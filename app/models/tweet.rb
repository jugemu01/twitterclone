class Tweet < ApplicationRecord
  belongs_to :user
  has_many :favorites
  has_many :comments

  validates :content, presence: true

  def favorited_by?(user)
    favorites.where(user_id: user.id).exists?
  end
end
