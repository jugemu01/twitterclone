class TweetsController < ApplicationController
  before_action :authenticate_user!, except: [:index]

  def index
    @tweet = Tweet.new
    @tweets = Tweet.all.order(created_at: "DESC")
  end

  def create
    @tweet = Tweet.new(tweets_params)
    @tweet.user_id = current_user.id
    if @tweet.save
      redirect_to tweets_path
    else
      @tweets = Tweet.all
      render :index
    end
  end

  def show
    @tweet = Tweet.find(params[:id])
    @comment = Comment.new
  end

  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
  end

  private

  def tweets_params
    params.require(:tweet).permit(:content, :user_id)
  end
end
