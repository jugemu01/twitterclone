class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @tweets = Tweet.where(user_id: @user.id)
    @favorite_tweets = @user.favorite_tweets
    @comments_tweets = @user.comments_tweets
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:notice] = 'Successfully edited.'
      redirect_to user_path(@user)
    else
      render :edit
    end
  end

  def follows
    @users = User.find(params[:id]).followings
  end

  def followers
    @users = User.find(params[:id]).followers
  end

  private

  def user_params
    params.require(:user).permit(:name, :user_image, :introduction)
  end
end
